<?php

/**
 * @file
 * GLightbox theme functions.
 */

use Drupal\Component\Utility\Crypt;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Entity\EntityInterface;
use Drupal\responsive_image\Entity\ResponsiveImageStyle;

/**
 * Prepares variables for glightbox formatter templates.
 *
 * Default template: glightbox-formatter.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - item: An ImageItem object.
 *   - item_attributes: An optional associative array of html attributes to be
 *     placed in the img tag.
 *   - entity: An entity object.
 *   - settings: Formatter settings array.
 *
 * @codingStandardsIgnoreStart
 */
function template_preprocess_glightbox_formatter(&$variables) {
  // @codingStandardsIgnoreEnd

  $item = $variables['item'];
  $item_attributes = $variables['item_attributes'] ?? [];
  $entity = $variables['entity'];
  $settings = $variables['settings'];
  $image_uri = $item->entity->getFileUri();
  $classes_array = ['glightbox'];
  $data_gbox_img_attrs = [];

  $parent_entity_id = '';
  $parent_paragraph_id = '';
  if (!empty($entity->_referringItem)) {
    $parent = $entity->_referringItem->getEntity();
    if ($parent instanceof EntityInterface) {
      $parent_entity_id = $parent->id();
    }

    if ($parent->getEntityTypeId() == 'paragraph') {
      $paragraph_parent = $parent->getParentEntity();
      if ($paragraph_parent instanceof EntityInterface) {
        $parent_paragraph_id = $paragraph_parent->id();
      }
    }
  }

  $gallery_id = \Drupal::service('glightbox.gallery_id_generator')->generateId($entity, $item, $settings, $parent_entity_id, $parent_paragraph_id);

  // Set up the $variables['image'] parameter.
  if ($settings['style_name'] == 'hide') {
    $variables['image'] = [];
    $classes_array[] = 'js-hide';
  }
  elseif (!empty($settings['style_name'])) {
    $variables['image'] = [
      '#theme' => 'image_style',
      '#style_name' => $settings['style_name'],
    ];
  }
  else {
    $variables['image'] = [
      '#theme' => 'image',
    ];
  }

  if (!empty($variables['image'])) {
    $variables['image']['#attributes'] = $item_attributes;

    // Do not output an empty 'title' attribute.
    if (!empty($item->title)) {
      $variables['image']['#title'] = $item->title;
      $data_gbox_img_attrs['title'] = '"title":"' . $item->title . '"';
    }

    foreach (['width', 'height', 'alt'] as $key) {
      $variables['image']["#$key"] = $item->$key;
      if ($key == 'alt') {
        $data_gbox_img_attrs['alt'] = '"alt":"' . $item->alt . '"';
      }
    }

    $variables['image']['#uri'] = empty($item->uri) ? $image_uri : $item->uri;
  }

  if (!empty($settings['glightbox_image_style'])) {
    $style = ImageStyle::load($settings['glightbox_image_style']);
    $variables['url'] = $style->buildUrl($image_uri);
  }
  else {
    /** @var \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator */
    $file_url_generator = \Drupal::service('file_url_generator');

    $variables['url'] = $file_url_generator->generateAbsoluteString($image_uri);
  }

  $caption = _glightbox_formatter_get_caption($variables);
  if (!empty($caption)) {
    $variables['attributes']['title'] = $caption;
    $variables['attributes']['data-glightbox'] = "title: $caption";
  }

  $variables['attributes']['data-gallery'] = $gallery_id;
  $variables['attributes']['class'] = $classes_array;
  if (!empty($data_gbox_img_attrs)) {
    $variables['attributes']['data-gbox-img-attrs'] = '{' . implode(',', $data_gbox_img_attrs) . '}';
  }

  if ($caption_description = _glightbox_formatter_get_caption($variables, 'glightbox_caption_description', FALSE)) {
    $variables['caption_description'] = $caption_description;
    $variables['attributes']['data-glightbox'] = "description: $caption_description";
  }
}

/**
 * Prepares variables for glightbox responsive formatter templates.
 *
 * Default template: glightbox-responsive-formatter.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - item: An ImageItem object.
 *   - item_attributes: An optional associative array of html attributes to be
 *     placed in the img tag.
 *   - entity: An entity object.
 *   - settings: Formatter settings array.
 */
function template_preprocess_glightbox_responsive_formatter(array &$variables) {
  $glightbox_inline = \Drupal::moduleHandler()->moduleExists('glightbox_inline');

  $item = $variables['item'];
  $item_attributes = $variables['item_attributes'] ?? [];
  $entity = $variables['entity'];
  $entity_bundle = $entity->bundle();
  $id = $entity->id();
  $entity_id = !empty($id) ? $entity_bundle . '-' . $id : 'entity-id';

  $settings = $variables['settings'];
  $image_uri = $item->entity->getFileUri();
  $classes_array = ['glightbox'];
  $data_gbox_img_attrs = [];

  $responsive_style = NULL;
  if (!empty($settings['glightbox_responsive_node_style'])) {
    $responsive_style = ResponsiveImageStyle::load($settings['glightbox_responsive_node_style']);
  }

  // Set up the $variables['responsive_image'] parameter.
  if ($settings['glightbox_responsive_node_style'] == 'hide') {
    $variables['responsive_image'] = [];
    $classes_array[] = 'js-hide';
  }
  elseif ($responsive_style) {
    $variables['responsive_image'] = [
      '#theme' => 'responsive_image',
      '#responsive_image_style_id' => $settings['glightbox_responsive_node_style'],
    ];
  }
  else {
    $variables['responsive_image'] = [
      '#theme' => 'image',
    ];
  }

  if (!empty($variables['responsive_image'])) {
    $attributes = [];
    // Do not output an empty 'title' attribute.
    if (mb_strlen($item->title ?? '') != 0) {
      $variables['responsive_image']['#title'] = $item->title;
      $data_gbox_img_attrs['title'] = '"title":"' . $item->title . '"';
      $attributes['title'] = $item->title;
    }

    foreach (['width', 'height', 'alt'] as $key) {
      $variables['responsive_image']["#$key"] = $item->$key;
    }
    $data_gbox_img_attrs['alt'] = '"alt":"' . $item->alt . '"';
    $attributes['alt'] = $item->alt;

    $variables['responsive_image']['#uri'] = empty($item->uri) ? $image_uri : $item->uri;
    $variables['responsive_image']['#attributes'] = $attributes + $item_attributes;
  }

  $responsive_style = NULL;
  if (!empty($settings['glightbox_responsive_image_style'])) {
    $responsive_style = ResponsiveImageStyle::load($settings['glightbox_responsive_image_style']);
  }

  // Since responsive images don't have an external url, link to internal
  // content.
  if ($glightbox_inline && $responsive_style) {
    // Create a unique internal link for the picture tag.
    // We use a short token since randomness is not critical.
    $image_id = $entity_id . '-' . Crypt::randomBytesBase64(8);
    $variables['image_id'] = $image_id;
    $variables['url'] = '';
    $variables['responsive_image'] = [
      '#theme' => 'responsive_image',
      '#responsive_image_style_id' => $settings['glightbox_responsive_image_style'],
      '#uri' => empty($item->uri) ? $image_uri : $item->uri,
      '#width' => $item->width,
      '#height' => $item->height,
    ];
    $variables['attributes']['data-glightbox-inline'] = '#' . $image_id;
  }
  if (!empty($settings['glightbox_image_style'])) {
    $style = ImageStyle::load($settings['glightbox_image_style']);
    $variables['url'] = $style->buildUrl($image_uri);
  }
  else {
    /** @var \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator */
    $file_url_generator = \Drupal::service('file_url_generator');

    $variables['url'] = $file_url_generator->generateAbsoluteString($image_uri);
  }

  $gallery_id = \Drupal::service('glightbox.gallery_id_generator')->generateId($entity, $item, $settings);

  $caption = _glightbox_formatter_get_caption($variables);
  if (!empty($caption)) {
    $variables['attributes']['title'] = $caption;
    $variables['attributes']['data-glightbox'] = "title: $caption";
  }
  $variables['attributes']['data-gallery'] = $gallery_id;
  $variables['attributes']['class'] = $classes_array;
  if (!empty($data_gbox_img_attrs)) {
    $variables['attributes']['data-gbox-img-attrs'] = '{' . implode(',', $data_gbox_img_attrs) . '}';
  }

  if ($caption_description = _glightbox_formatter_get_caption($variables, 'glightbox_caption_description', FALSE)) {
    $variables['caption_description'] = $caption_description;
    $variables['attributes']['data-glightbox'] = "description: $caption_description";
  }
}

/**
 * Get the caption for an image.
 *
 * @param array $variables
 *   An associative array containing:
 *   - item: An ImageItem object.
 *   - item_attributes: An optional associative array of html attributes to be
 *     placed in the img tag.
 *   - entity: An entity object.
 *   - settings: Formatter settings array.
 * @param string $caption_field
 *   The caption field to use.
 * @param bool $filter
 *   Whether to filter the caption text.
 *
 * @return string
 *   The caption text of the image parameter.
 */
function _glightbox_formatter_get_caption(array &$variables, string $caption_field = 'glightbox_caption', bool $filter = TRUE): string {
  $item = $variables['item'];
  $entity = $variables['entity'];
  $settings = $variables['settings'];

  // Build the caption.
  $entity_title = $entity->label();
  $entity_type = $entity->getEntityTypeId();

  switch ($settings[$caption_field]) {
    case 'auto':
      // If the title is empty use alt or the entity title in that order.
      if (!empty($item->title)) {
        $caption = $item->title;
      }
      elseif (!empty($item->alt)) {
        $caption = $item->alt;
      }
      elseif (!empty($entity_title)) {
        $caption = $entity_title;
      }
      else {
        $caption = '';
      }
      break;

    case 'title':
      $caption = $item->title;
      break;

    case 'alt':
      $caption = $item->alt;
      break;

    case 'entity_title':
      $caption = $entity_title;
      break;

    case 'custom':
      $token_service = \Drupal::token();
      $data = [$entity_type => $entity, 'file' => $item];
      if (!empty($entity->_referringItem)) {
        $parent = $entity->_referringItem->getEntity();
        if ($entity_type != $parent->getEntityTypeId()) {
          // Add token for Entity -> Media.
          $data[$parent->getEntityTypeId()] = $parent;

          if ($parent->getEntityTypeId() == 'paragraph') {
            $entity_container = $parent->getParentEntity();
            if (!empty($entity_container) && $entity_container->getEntityTypeId() == 'node') {
              // Add token for Node -> Paragraph -> Media.
              $data['node'] = $entity_container;
            }
            elseif (!empty($entity_container) && $entity_container->getEntityTypeId() == 'paragraph') {
              // Add token for Paragraph -> Paragraph -> Media.
              $data['paragraph_container'] = $entity_container;
              $node_container = $entity_container->getParentEntity();
              if (!empty($node_container) && $node_container->getEntityTypeId() == 'node') {
                // Add token for Node -> Paragraph -> Paragraph -> Media.
                $data['node'] = $node_container;
              }
            }
          }
        }
        else {
          // Add token for Paragraph -> Paragraph -> Image.
          $data['paragraph_container'] = $parent;
          if ($parent->getEntityTypeId() == 'paragraph') {
            $entity_container = $parent->getParentEntity();
            if (!empty($entity_container) && $entity_container->getEntityTypeId() == 'node') {
              // Add token for Node -> Paragraph -> Paragraph -> Image.
              $data['node'] = $entity_container;
            }
          }
        }

      }
      $caption = $token_service->replace(
        $settings["{$caption_field}_custom"],
        $data,
        ['clear' => FALSE]
      );
      break;

    default:
      $caption = '';
  }

  // If File Entity module is enabled, load attribute values from file entity.
  if (\Drupal::moduleHandler()->moduleExists('file_entity')) {
    // File id of the save file.
    $fid = $item->target_id;
    // Load file object.
    $file_obj = File::load($fid);
    $file_array = $file_obj->toArray();
    // Populate the image title.
    if (!empty($file_array['field_image_title_text'][0]['value']) && empty($item->title) && $settings[$caption_field] == 'title') {
      $caption = $file_array['field_image_title_text'][0]['value'];
    }
    // Populate the image alt text.
    if (!empty($file_array['field_image_alt_text'][0]['value']) && empty($item->alt) && $settings[$caption_field] == 'alt') {
      $caption = $file_array['field_image_alt_text'][0]['value'];
    }
  }

  // Shorten the caption for the example styles or when caption
  // shortening is active.
  $config = \Drupal::config('glightbox.settings');
  $glightbox_style = !empty($config->get('glightbox_style')) ? $config->get('glightbox_style') : '';
  $trim_length = $config->get('glightbox_caption_trim_length');
  if (((strpos($glightbox_style, 'glightbox/example') !== FALSE) || $config->get('glightbox_caption_trim')) && (strlen($caption) > $trim_length)) {
    $caption = substr($caption, 0, $trim_length - 5) . '...';
  }
  if ($filter) {
    return Xss::filter($caption);
  }
  return $caption;
}
